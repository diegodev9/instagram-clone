class PicsController < ApplicationController
  before_action :set_pic, only: [:show, :edit, :update, :destroy, :upvote]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @pics = Pic.all.order(created_at: :desc)
  end

  def new
    @pic = Pic.new
  end

  def show
  end

  def edit
  end

  def create
    @pic = Pic.new(pic_params)
    @pic.user = current_user

    if @pic.save
      redirect_to pic_path(@pic), notice: 'Pic created'
    else
      render 'new'
    end
  end

  def update
    if @pic.update(pic_params)
      redirect_to pic_path(@pic), notice: 'Pic updated'
    else
      render 'edit'
    end
  end

  def destroy
    @pic.image.purge
    @pic.destroy
    redirect_to root_path
  end

  def upvote
    @pic.upvote_by current_user
    redirect_to @pic
  end

  private

  def set_pic
    @pic = Pic.find(params[:id])
  end

  def pic_params
    params.require(:pic).permit(:title, :description, :image)
  end
end
