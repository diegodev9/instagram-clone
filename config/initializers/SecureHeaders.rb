SecureHeaders::Configuration.default do |config|

  config.csp = {
    default_src: %w('self'),
    script_src: %w('self' 'unsafe-inline'),
    style_src: %w('self' 'unsafe-inline'),
    img_src: %w('self' data:),
    img_src: %w('self' https://res.cloudinary.com/dsup5jr5d/image/upload/v1/micaleta/)
  }

end