const { environment } = require('@rails/webpacker')

// Add an additional plugin of your choosing : ProvidePlugin
const webpack = require("webpack");
environment.plugins.prepend(
    "Provide",
    new webpack.ProvidePlugin({
        $: 'jquery/src/jquery',
        jQuery: 'jquery/src/jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery'
        //Popper: ["popper.js", "default"] // for Bootstrap 4
    })
);

module.exports = environment
